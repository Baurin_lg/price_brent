#curl -s http://server/path/script.sh | bash /dev/stdin arg1 arg2

import urllib
from lxml import html
import argparse
import sys

parser = argparse.ArgumentParser(description='Process some integers.')
parser.add_argument('-max', type=int,
                   help='Max Date for extract data. (<2018)')

parser.add_argument('-min',  type=int,
                   help='Min Date for extract data (>2004)')

args = parser.parse_args()

anos = [args.min, args.max]



print "Imprimiento ..."

for ano in range(anos[0]-1,anos[1]):

	print "                 \033[31mAno %s\033[0m"%ano
	url = "https://www.datosmacro.com/energia/precios-gasolina-diesel-calefaccion/espana?anio=%s"%ano
	page = html.fromstring(urllib.urlopen(url).read())

	for tr in page.xpath("//*[contains(@class, 'tabledat')]//thead//tr"):

		line = ""
		for index,th in enumerate(tr.findall("th")):
			line += "-\033[41m%s\033[0m-"%th.text_content()

        print line


	for tr in page.xpath("//*[contains(@class, 'tabledat')]//tbody//tr"):
        

		print tr.text_content()
        
